#!/bin/bash

. `dirname $0`/sync.ini

for ROLE in ${!ROLES[@]}; do
    echo "Start to sync ${ROLE}"
    if [ ! -d "${MIRROR}/${ROLE}" ]; then
        git clone ${SRC_URL}/${ROLES[${ROLE}]}.git ${MIRROR}/${ROLE}
    fi
    cd ${MIRROR}/${ROLE}
    git remote set-url --push origin ${DST_URL}/${ROLE}.git
    git pull
    git push
done
